﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;

namespace Graphics
{
    public class DelegateCommand : ICommand
    {
        private readonly Action _execute;
        public DelegateCommand(Action execute)
        {
            _execute = execute;
        }

        public void Execute(object parameter = null)
        {
            _execute();
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void RaiseCanExecuteChanged()
        {
            if(CanExecuteChanged != null)
                CanExecuteChanged(this, new EventArgs());
        }

        public event EventHandler CanExecuteChanged;
    }
}
