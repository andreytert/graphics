﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OxyPlot.Series;
using Graphics.Formuls;
using OxyPlot;

namespace Graphics
{
    class DrawHelper
    {
        public LineSeries AditionalHWithFUseGamma2DualReciveDraw(double start, double end, double step, AdditionalH additionalH, Sigma2 sgm2)
        {
            LineSeries lineSeries = new LineSeries();

            var range = RangeHelper.GenerateRange(start, end, step);

            foreach (var f in range)
            {
                double y = MathHelper.ToDb(additionalH.CalculateWithFUseGamma2Dual(f, sgm2));
                if (y < 0.0)
                    break;
                lineSeries.Points.Add(new DataPoint(f, y));
            }
            return lineSeries;
        }

        public LineSeries AditionalHWithF1DualReciveDraw(double start, double end, double step, AdditionalH additionalH)
        {
            LineSeries lineSeries = new LineSeries();

            var range = RangeHelper.GenerateRange(start, end, step);

            foreach (var d in range)
            {
                double y = MathHelper.ToDb(additionalH.CalculateWithF1Dual(d));
                if (y < 0.0)
                    break;
                lineSeries.Points.Add(new DataPoint(d, y));
            }
            return lineSeries;
        }

        public LineSeries AditionalHWithF2DualReciveDraw(double start, double end, double step, AdditionalH additionalH)
        {
            LineSeries lineSeries = new LineSeries();

            var range = RangeHelper.GenerateRange(start, end, step);

            foreach (var d in range)
            {
                double y = MathHelper.ToDb(additionalH.CalculateWithF2Dual(d));
                if (y < 0.0)
                    break;
                lineSeries.Points.Add(new DataPoint(d, y));
            }

            return lineSeries;
        }

        public LineSeries AdditionalHWithFUseGamma2SingReciveDraw(double start, double end, double step, AdditionalH additionalH, Sigma2 sgm2)
        {
            LineSeries ls = new LineSeries();

            var range = RangeHelper.GenerateRange(start, end, step);
            foreach (var f in range)
            {
                double y = additionalH.CalculateWithFUseGamma2Single(f, sgm2);
                y = MathHelper.ToDb(y);
                if (y < 0.0)
                    break;
                ls.Points.Add(new DataPoint { X = f, Y = y });
            }
            return ls;
        }

        public LineSeries AdditionalHWithF1SingReciveDraw(double start, double end, double step, AdditionalH additionalH)
        {
            LineSeries ls = new LineSeries();

            var range = RangeHelper.GenerateRange(start, end, step);
            foreach (var g in range)
            {
                double y = additionalH.CalculateUseF1Single(g);
                y = MathHelper.ToDb(y);
                if (y < 0.0)
                    break;
                ls.Points.Add(new DataPoint { X = g, Y = y });
            }

            return ls;
        }

        public LineSeries AdditionalHWithF2SingReciveDraw(double start, double end, double step, AdditionalH additionalH)
        {
            LineSeries ls = new LineSeries();

            var range = RangeHelper.GenerateRange(start, end, step);
            foreach (var g in range)
            {
                double y = additionalH.CalculateUseF2Single(g);
                y = MathHelper.ToDb(y);
                if (y < 0.0)
                    break;
                ls.Points.Add(new DataPoint { X = g, Y = y });
            }
            return ls;
        }
    }
}
