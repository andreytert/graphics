﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Graphics.Formuls
{
    public class AdditionalH
    {
        public double _R { get; set; }
        public double PErrAdditional { get; set; }

        public AdditionalH(double R = 0.5)
        {
            _R = R;
            PErrAdditional = 1e-5;
        }

        public double CalculateWithGammaDual(double sqrGamma)
        {
            double ans = 3.0 * Math.Pow(1 + sqrGamma, 2) / (1 - _R * _R) * Math.Exp(-2.0 * sqrGamma / (1 + _R)) / PErrAdditional;
            ans = Math.Sqrt(ans);
            return ans;
        }

        public double CalculateWithFUseGamma2Dual(double f, Sigma2 sgm2)
        {
            GammaUseSigma2 gamma = new GammaUseSigma2();
            double sqrGamma = gamma.CalculateUseF(f, sgm2);
            return CalculateWithGammaDual(sqrGamma);
        }

        public double CalculateWithF1Dual(double f)
        {
            Gamma1 gamma1 = new Gamma1();
            double sqrGamma = gamma1.CalculateUseF(f);
            return CalculateWithGammaDual(sqrGamma);
        }

        public double CalculateWithF2Dual(double f)
        {
            Gamma2 gamma2 = new Gamma2();
            double sqrGamma = gamma2.CalculateUseF(f);
            return CalculateWithGammaDual(sqrGamma);
        }

        public double CalculateUseGammaSingle(double gamma)
        {
            double tmp = Math.Exp(-gamma);
            double ans = (1 + gamma) * Math.Exp(-gamma) / 1e-5;
            return ans;
        }

        public double CalculateWithFUseGamma2Single(double f, Sigma2 sgm2)
        {
            GammaUseSigma2 gamma = new GammaUseSigma2();
            double sqrGamma = gamma.CalculateUseF(f, sgm2);
            return CalculateUseGammaSingle(sqrGamma);
        }

        public double CalculateUseF1Single(double f)
        {
            Gamma1 gamma1 = new Gamma1();
            double gamma = gamma1.CalculateUseF(f);
            //double ans = (1 + gamma) * Math.Exp(-gamma) / 1e-5;
            return CalculateUseGammaSingle(gamma);
        }

        public double CalculateUseF2Single(double f)
        {
            Gamma2 gamma2 = new Gamma2();
            double gamma = gamma2.CalculateUseF(f);
            //double ans = (1 + gamma) * Math.Exp(-gamma) / 1e-5;
            return CalculateUseGammaSingle(gamma);
        }
    }
}
