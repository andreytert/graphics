﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Graphics.Formuls
{
    public class Gamma1
    {
        public double CalculateUseF(double f)
        {
            Sigma1 sgm = new Sigma1();
            return CalculateUseSigma(sgm.Calculate(f));
        }

        public double CalculateUseSigma(double sgm)
        {
            double ans = 1.0 / (Math.Exp(Math.Pow(sgm, 2.0)) - 1);
            return ans;
        }
    }
}
