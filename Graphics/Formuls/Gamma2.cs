﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Graphics.Formuls
{
    public class Gamma2
    {
        public double CalculateUseF(double f)
        {
            Sigma2u sgm = new Sigma2u();
            return CalculateUseSigma(sgm.Calculate(f));
        }

        public double CalculateUseSigma(double sgm)
        {
            double ans = 1.0 / (Math.Exp(Math.Pow(sgm, 2.0)) - 1);
            return ans;
        }
    }
}
