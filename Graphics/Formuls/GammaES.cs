﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Graphics.Formuls
{
    public class GammaEnergySecrecy
    {
        public double R { get; set; }
        public double PErrAddit { get; set; }

        public GammaEnergySecrecy()
        {
            R = 0.5;
            PErrAddit = 1e-5;
        }

        public double Calculate(double gamma)
        {
            double ans = Math.Sqrt((1 - R*R)/(3*PErrAddit))*Math.Exp(-gamma*gamma*R/(1 + R));
            return ans;
        }
    }
}
