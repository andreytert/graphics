﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Graphics.Formuls
{
    public class GammaUseSigma2
    {
        public double CalculateUseF(double f, Sigma2 sgm2)
        {
            return CalculateUseSigma(sgm2.Calculate(f));
        }

        public double CalculateUseSigma(double sgm)
        {
            double ans = 1.0 / (Math.Exp(Math.Pow(sgm, 2.0)) - 1);
            return ans;
        }
    }
}
