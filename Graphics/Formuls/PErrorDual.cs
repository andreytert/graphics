﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Graphics.Formuls
{
    public class PErrorDual
    {
        private double _gamma;

        public double R { get; set; }

        public PErrorDual()
        {
            _gamma = 1;
            R = 0.5;
        }

        public double Calcute(double h)
        {
            double ans = 3.0*(1 + Gamma)/(1 - R*R)/h/h*Math.Exp(-2.0*Gamma/(1 + R));
            return ans;
        }

        public double Gamma
        {
            get { return _gamma; }
            set
            {
                _gamma = value;
            }
        }
    }
}
