﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Graphics.Formuls
{
    /// <summary>
    /// Approximate Formula
    /// </summary>
    public class PErrorSingle
    {
        private double _gamma;

        public PErrorSingle()
        {
            _gamma = 1;
        }

        public double Calcute(double h)
        {
            double ans = (1.0 + _gamma) * Math.Exp(-_gamma) / h;
            return ans;
        }

        public double Gamma
        {
            get { return _gamma; }
            set
            {
                _gamma = value;
            }
        }
    }
}
