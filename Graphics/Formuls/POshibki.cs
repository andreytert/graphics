﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Graphics.Formuls
{
    class POshibki
    {
        private double _gamma;

        public POshibki()
        {
            _gamma = 1;
        }

        public double Calcute(double h)
        { 
            double denominator=h+2*(_gamma+1);
            double pOsh= (_gamma+1)/denominator* Math.Exp(-1*_gamma*h/denominator);
            return pOsh;
        }

        public double Gamma
        {
            get { return _gamma; }
            set
            {
                _gamma = value;
            }
        }
    }
}
