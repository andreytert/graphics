﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Graphics.Formuls
{
    public class Sigma
    {
        protected double alph, L0, k, Nt, B, deltH;

        public Sigma(double alph, double L0)
        {
            this.alph = MathHelper.toRad(alph);
            this.L0 = L0;
            this.k = 1.2e-6;
            this.B = 3e-3;
            this.Nt = 4e17;
            this.deltH = 5.5e5;
            
            
        }
    }
}
