﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Graphics.Formuls
{
    public class Sigma2 : Sigma
    {
        public Sigma2(double alph, double L0) : base(alph, L0)
        {
        }

        public double Calculate(double f0)
        {
            double ans = k * Math.Sqrt(L0 / (2 * deltH) * (1.0 / Math.Sin(alph)))* (Nt * B / f0);
            return ans;
        }
    }
}
