﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Graphics.Formuls
{
    public class Sigma2u
    {
        public double Calculate(double f0)
        {
            double ans = 9.7 * 1e7 / f0;
            return ans;
        }
    }
}
