﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Graphics
{
    class MathHelper
    {
        static public double ToDb(double val)
        {
            return 10*Math.Log10(val);
        }

        static public double toRad(double ang)
        {
            return ang * Math.PI / 180.0;
        }
    }
}
