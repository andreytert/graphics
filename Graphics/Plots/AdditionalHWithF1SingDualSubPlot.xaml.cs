﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Graphics.ViewModels;

namespace Graphics.Plots
{
    /// <summary>
    /// Interaction logic for AdditionalHWithF1SingDualSubPlot.xaml
    /// </summary>
    public partial class AdditionalHWithF1SingDualSubPlot : UserControl
    {
        AdditionalHWithF1SingDualSubPlotViewModel additionalHWithF1SingDualSubPlotViewModel =
            new AdditionalHWithF1SingDualSubPlotViewModel();
        public AdditionalHWithF1SingDualSubPlot()
        {
            InitializeComponent();
            DataContext = additionalHWithF1SingDualSubPlotViewModel;
        }
    }
}
