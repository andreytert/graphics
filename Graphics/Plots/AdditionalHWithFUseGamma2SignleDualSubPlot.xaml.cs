﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Graphics.ViewModels;

namespace Graphics.Plots
{
    /// <summary>
    /// Interaction logic for AdditionalHWithFUseGamma2SignleDualSubPlot.xaml
    /// </summary>
    public partial class AdditionalHWithFUseGamma2SignleDualSubPlot : UserControl
    {
        AdditionalHWithFUseGamma2SingleDualSubPlotViewModel viewModel = new AdditionalHWithFUseGamma2SingleDualSubPlotViewModel();
        public AdditionalHWithFUseGamma2SignleDualSubPlot()
        {
            InitializeComponent();
            DataContext = viewModel;
        }
    }
}
