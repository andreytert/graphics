﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Graphics.ViewModels;

namespace Graphics.Plots
{
    /// <summary>
    /// Interaction logic for AditionalHWithF1DualRecivePlot.xaml
    /// </summary>
    public partial class AditionalHWithF1DualRecivePlot : UserControl
    {
        private AditionalHWithF1DualRecivePlotViewModel _model = new AditionalHWithF1DualRecivePlotViewModel();
        public AditionalHWithF1DualRecivePlot()
        {
            InitializeComponent();
            DataContext = _model;
        }
    }
}
