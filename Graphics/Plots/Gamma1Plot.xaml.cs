﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Graphics.ViewModels;

namespace Graphics.Plots
{
    /// <summary>
    /// Interaction logic for Gamma1Plot.xaml
    /// </summary>
    public partial class Gamma1Plot : UserControl
    {
        private Gamma1PlotViewModel viewModel = new Gamma1PlotViewModel();

        public Gamma1Plot()
        {
            InitializeComponent();
            DataContext = viewModel;
        }
    }
}
