﻿using Graphics.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Graphics.Plots
{
    /// <summary>
    /// Interaction logic for Gamma2Plot.xaml
    /// </summary>
    public partial class Gamma2Plot : UserControl
    {
        private Gamma2PlotViewModel viewModel = new Gamma2PlotViewModel();

        public Gamma2Plot()
        {
            InitializeComponent();
            DataContext = viewModel;
        }
    }
}
