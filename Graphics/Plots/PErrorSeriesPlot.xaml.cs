﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Graphics.ViewModels;

namespace Graphics.Plots
{
    /// <summary>
    /// Логика взаимодействия для PErrorSeriesPlot.xaml
    /// </summary>
    public partial class PErrorSeriesPlot : UserControl
    {
        private PErrorSeriesPlotViewModel _viewModel = new PErrorSeriesPlotViewModel();

        public PErrorSeriesPlot()
        {
            InitializeComponent();
            DataContext = _viewModel;
        }
    }
}
