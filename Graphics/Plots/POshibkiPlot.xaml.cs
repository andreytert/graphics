﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Graphics.ViewModels;

namespace Graphics.Plots
{
    /// <summary>
    /// Interaction logic for POshibkiPlot.xaml
    /// </summary>
    public partial class POshibkiPlot : UserControl
    {
        private POshibkiViewModel _model = new POshibkiViewModel();
        public POshibkiPlot()
        {
            InitializeComponent();
            DataContext = _model;
        }
    }
}
