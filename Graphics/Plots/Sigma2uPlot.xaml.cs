﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Graphics.ViewModels;

namespace Graphics.Plots
{
    /// <summary>
    /// Interaction logic for Sigma1Plot.xaml
    /// </summary>
    public partial class Sigma2Plot : UserControl
    {
        private Sigma2uPlotViewModel _model = new Sigma2uPlotViewModel();

        public Sigma2Plot()
        {
            InitializeComponent();
            DataContext = _model;
        }
    }
}
