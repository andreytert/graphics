﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Graphics
{
    class RangeHelper
    {
        public static IEnumerable<double> GenerateRange(double from, double to, double step)
        {
            List<double> res = new List<double>();
            for (double i = from; i <= to; i += step)
            {
                res.Add(i);
            }
            return res;
        }

        public static IEnumerable<double> GenerateRangeF0()
        {
            List<double> res = new List<double>()
            {
                1e9, 5e8, 4e8, 2.5e8, 2e8, 1.5e8, 1e8, 8e7, 7e7, 6e7, 3e7
            };
            return res;
        }
    }
}
