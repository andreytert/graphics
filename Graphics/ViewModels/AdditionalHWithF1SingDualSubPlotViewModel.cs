﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Graphics.Formuls;
using OxyPlot;
using OxyPlot.Series;
using OxyPlot.Axes;

namespace Graphics.ViewModels
{
    class AdditionalHWithF1SingDualSubPlotViewModel : ViewModelBase
    {
        private AdditionalH _aditionalH;
        private PlotModel _plotModel;
        private DelegateCommand _drawCommand;


        public AdditionalHWithF1SingDualSubPlotViewModel()
        {
            _aditionalH = new AdditionalH();
            _drawCommand = new DelegateCommand(Draw);
        }

        private void Draw()
        {
            PlotModel newPlot = new PlotModel() { DefaultFontSize = 20, LegendFontSize = 20 };
            newPlot.Axes.Add(new LinearAxis(AxisPosition.Bottom) { Title = "частота (f1, Гц)", UseSuperExponentialFormat = true });
            newPlot.Axes.Add(new LinearAxis(AxisPosition.Left, 0, 55.0) { Title = "коэффициент глубины БЗ (" + Convert.ToChar(0x3B3) + "^2)" });

            DrawHelper drawHelper = new DrawHelper();
            LineSeries lineSeriesF1Dual = drawHelper.AditionalHWithF2DualReciveDraw(3e7, 2e9, 1e6, _aditionalH);
            lineSeriesF1Dual.Title = "f1 сдвоенный";
            LineSeries lineSeriesF1Sing = drawHelper.AdditionalHWithF2SingReciveDraw(3e7, 2e9, 1e6, _aditionalH);
            lineSeriesF1Sing.Title = "f1 одиночный";
            LineSeries lineResiesSub = new LineSeries();
            lineResiesSub.Title = "разность";
            var dual = lineSeriesF1Dual.Points;
            var single = lineSeriesF1Sing.Points;
            int sz = Math.Min(dual.Count, single.Count);
            for (int i = 0; i < sz; i++)
            {
                if (single[i].Y < dual[i].Y)
                    break;
                lineResiesSub.Points.Add(new DataPoint { X = dual[i].X, Y = single[i].Y - dual[i].Y });
            }
            lineResiesSub.LineStyle = LineStyle.Dot;

            newPlot.Series.Add(lineSeriesF1Dual);
            newPlot.Series.Add(lineSeriesF1Sing);
            newPlot.Series.Add(lineResiesSub);

            PlotModel = newPlot;
        }

        public DelegateCommand DrawCommand
        {
            get { return _drawCommand; }
        }

        public PlotModel PlotModel
        {
            get { return _plotModel; }
            set
            {
                if (Equals(value, _plotModel)) return;
                _plotModel = value;
                OnPropertyChanged("PlotModel");
            }
        }

        public AdditionalH AditionalH
        {
            get { return _aditionalH; }
        }
    }
}
