﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OxyPlot;
using Graphics.Formuls;
using OxyPlot.Series;
using OxyPlot.Axes;

namespace Graphics.ViewModels
{
    class AdditionalHWithF1SingPlotViewModel : ViewModelBase
    {
        private DelegateCommand drawCommand;

        public AdditionalHWithF1SingPlotViewModel()
        {
            additionalH = new AdditionalH();
            drawCommand = new DelegateCommand(Draw);
        }

        private PlotModel plotModel;
        public PlotModel PlotModel
        {
            get { return plotModel; }
            set
            {
                if (Equals(value, plotModel)) return;
                plotModel = value;
                OnPropertyChanged("PlotModel");
            }
        }

        private AdditionalH additionalH;

        private void Draw()
        {
            
            PlotModel newPlotModel = new PlotModel();
            
            
            newPlotModel.Axes.Add(new LinearAxis(AxisPosition.Left));
            newPlotModel.Axes.Add(new LinearAxis(AxisPosition.Bottom) { UseSuperExponentialFormat = true });

            DrawHelper drawHelper = new DrawHelper();

            LineSeries ls = drawHelper.AdditionalHWithF1SingReciveDraw(3e7, 2e9, 1e6, additionalH);
            
            newPlotModel.Series.Add(ls);
            PlotModel = newPlotModel;
        }

        public DelegateCommand DrawCommand
        {
            get { return drawCommand; }
        }
    }
}
