﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OxyPlot;
using OxyPlot.Series;
using OxyPlot.Axes;
using Graphics.Formuls;

namespace Graphics.ViewModels
{
    class AdditionalHWithF2SingPlotViewModel : ViewModelBase
    {
        private DelegateCommand drawCommand;

        public AdditionalHWithF2SingPlotViewModel()
        {
            additionalH = new AdditionalH();
            drawCommand = new DelegateCommand(Draw);
        }

        private PlotModel plotModel;
        public PlotModel PlotModel
        {
            get { return plotModel; }
            set
            {
                if (Equals(value, plotModel)) return;
                plotModel = value;
                OnPropertyChanged("PlotModel");
            }
        }

        private AdditionalH additionalH;

        private void Draw()
        {

            PlotModel newPlotModel = new PlotModel() { DefaultFontSize=20, LegendFontSize=20};

            DrawHelper drawHelper = new DrawHelper();

            var ls = drawHelper.AdditionalHWithF2SingReciveDraw(3e7, 2e9, 1e6, additionalH);
            ls.Title = "hдоп2";
            newPlotModel.Axes.Add(new LinearAxis(AxisPosition.Bottom) { Title = "частота (f2, Гц)", UseSuperExponentialFormat = true });
            newPlotModel.Axes.Add(new LinearAxis(AxisPosition.Left) { Title = "коэффициент глубины БЗ (" + Convert.ToChar(0x3B3) + "^2)" });
            
            newPlotModel.Series.Add(ls);
            PlotModel = newPlotModel;
        }

        public DelegateCommand DrawCommand
        {
            get { return drawCommand; }
        }
    }
}
