﻿using Graphics.Formuls;
using OxyPlot;
using OxyPlot.Axes;
using OxyPlot.Series;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Graphics.ViewModels
{
    class AdditionalHWithFUseGamma2SingleDualSubPlotViewModel : ViewModelBase
    {
        private AdditionalH _aditionalH;
        private PlotModel _plotModel;
        private DelegateCommand _drawCommand;


        public AdditionalHWithFUseGamma2SingleDualSubPlotViewModel()
        {
            _aditionalH = new AdditionalH();
            _drawCommand = new DelegateCommand(Draw);
        }

        private void Draw()
        {
            PlotModel newPlot = new PlotModel() { DefaultFontSize=20, LegendFontSize=20};
            newPlot.Axes.Add(new LinearAxis(AxisPosition.Left, 0, 55.0) { Title = "коэффициент глубины БЗ (" + Convert.ToChar(0x3B3) + "^2)" });
            newPlot.Axes.Add(new LinearAxis(AxisPosition.Bottom) { UseSuperExponentialFormat = true, Title = Convert.ToChar(0x3B3).ToString() });

            DrawHelper drawHelper = new DrawHelper();
            Sigma2 sgm2 = new Sigma2(25, 1000);
            LineSeries lineSeriesFDual = drawHelper.AditionalHWithFUseGamma2DualReciveDraw(3e7, 2e9, 1e6, _aditionalH, sgm2);
            lineSeriesFDual.Title = "сдвоенный";
            LineSeries lineSeriesFSing = drawHelper.AdditionalHWithFUseGamma2SingReciveDraw(3e7, 2e9, 1e6, _aditionalH, sgm2);
            lineSeriesFSing.Title = "одиночный";
            LineSeries lineResiesSub = new LineSeries();
            lineResiesSub.Title = "разность";
            var dual = lineSeriesFDual.Points;
            var single = lineSeriesFSing.Points;
            int sz = Math.Min(dual.Count, single.Count);
            for (int i = 0; i < sz; i++)
            {
                if (single[i].Y < dual[i].Y)
                    break;
                lineResiesSub.Points.Add(new DataPoint { X = dual[i].X, Y = single[i].Y - dual[i].Y });
            }
            lineResiesSub.LineStyle = LineStyle.Dot;

            newPlot.Series.Add(lineSeriesFDual);
            newPlot.Series.Add(lineSeriesFSing);
            newPlot.Series.Add(lineResiesSub);

            PlotModel = newPlot;
        }

        public DelegateCommand DrawCommand
        {
            get { return _drawCommand; }
        }

        public PlotModel PlotModel
        {
            get { return _plotModel; }
            set
            {
                if (Equals(value, _plotModel)) return;
                _plotModel = value;
                OnPropertyChanged("PlotModel");
            }
        }

        public AdditionalH AditionalH
        {
            get { return _aditionalH; }
        }
    }
}
