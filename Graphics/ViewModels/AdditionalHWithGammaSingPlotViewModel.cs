﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Graphics.Formuls;
using OxyPlot;
using OxyPlot.Series;
using OxyPlot.Axes;

namespace Graphics.ViewModels
{
    class AdditionalHWithGammaSingPlotViewModel : ViewModelBase
    {
        private DelegateCommand drawCommand;

        public AdditionalHWithGammaSingPlotViewModel()
        {
            additionalH = new AdditionalH();
            drawCommand = new DelegateCommand(Draw);
        }

        private PlotModel plotModel;
        public PlotModel PlotModel
        {
            get { return plotModel; }
            set
            {
                if (Equals(value, plotModel)) return;
                plotModel = value;
                OnPropertyChanged("PlotModel");
            }
        }

        private AdditionalH additionalH;

        private void Draw()
        {

            PlotModel newPlotModel = new PlotModel() { DefaultFontSize = 20, LegendFontSize = 20 };
            LineSeries lineSeries = new LineSeries() { Title = "hдоп2" };
            newPlotModel.Axes.Add(new LinearAxis(AxisPosition.Left) { Title = "отношение сигнал/шум (hдоп2)" });
            newPlotModel.Axes.Add(new LinearAxis(AxisPosition.Bottom) { Title = "коэффициент глубины БЗ (" + Convert.ToChar(0x3B3) + "^2)" });
            var range = RangeHelper.GenerateRange(0, 20, 0.5);
            foreach (var g in range)
            {
                double y = additionalH.CalculateUseGammaSingle(g);
                y = MathHelper.ToDb(y);
                if (y < 0.0)
                    break;
                lineSeries.Points.Add(new DataPoint { X = g, Y = y });
            }

            newPlotModel.Series.Add(lineSeries);
            PlotModel = newPlotModel;
        }

        public DelegateCommand DrawCommand
        {
            get { return drawCommand; }
        }
    }
}
