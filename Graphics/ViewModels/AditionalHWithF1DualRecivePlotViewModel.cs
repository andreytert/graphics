﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Graphics.Formuls;
using OxyPlot;
using OxyPlot.Series;
using OxyPlot.Axes;

namespace Graphics.ViewModels
{
    class AditionalHWithF1DualRecivePlotViewModel : ViewModelBase
    {
        private AdditionalH _aditionalH;
        private PlotModel _plotModel;
        private DelegateCommand _drawCommand;


        public AditionalHWithF1DualRecivePlotViewModel()
        {
            _aditionalH = new AdditionalH();
            _drawCommand = new DelegateCommand(Draw);
        }

        private void Draw()
        {
            PlotModel newPlot = new PlotModel() { DefaultFontSize = 20, LegendFontSize = 20 };

            DrawHelper drawHelper = new DrawHelper();
            LineSeries lineSeries = drawHelper.AditionalHWithF1DualReciveDraw(3e7, 2e9, 1e6, _aditionalH);
            lineSeries.Title = "hдоп2";

            newPlot.Axes.Add(new LinearAxis(AxisPosition.Bottom) { Title = "частота (f2, Гц)", UseSuperExponentialFormat = true });
            newPlot.Axes.Add(new LinearAxis(AxisPosition.Left, 0, 40.0) { Title = "коэффициент глубины БЗ (" + Convert.ToChar(0x3B3) + "^2)" });
            newPlot.Series.Add(lineSeries);

            PlotModel = newPlot;
        }

        public DelegateCommand DrawCommand
        {
            get { return _drawCommand; }
        }

        public PlotModel PlotModel
        {
            get { return _plotModel; }
            set
            {
                if (Equals(value, _plotModel)) return;
                _plotModel = value;
                OnPropertyChanged("PlotModel");
            }
        }

        public AdditionalH AditionalH
        {
            get { return _aditionalH; }
        }
    }
}
