﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Graphics.Formuls;
using OxyPlot;
using OxyPlot.Axes;
using OxyPlot.Series;

namespace Graphics.ViewModels
{
    public class AditionalHWithGammaDualRecivePlotViewModel : ViewModelBase
    {
        private AdditionalH _aditionalH;
        private PlotModel _plotModel;
        private DelegateCommand _drawCommand;


        public AditionalHWithGammaDualRecivePlotViewModel()
        {
            _aditionalH = new AdditionalH();
            _drawCommand = new DelegateCommand(Draw);
        }

        private void Draw()
        {
            PlotModel newPlot = new PlotModel() { DefaultFontSize=20, LegendFontSize=20};
            LineSeries lineSeries = new LineSeries() {Title="hдоп2"};

            var range = RangeHelper.GenerateRange(0, 20, 0.1);

            foreach (var d in range)
            {
                lineSeries.Points.Add(new DataPoint(d, MathHelper.ToDb(_aditionalH.CalculateWithGammaDual(d))));
            }

            newPlot.Axes.Add(new LinearAxis(AxisPosition.Left){Title = "отношение сигнал/шум (hдоп2)"});
            newPlot.Axes.Add(new LinearAxis(AxisPosition.Bottom) { Title = "коэффициент глубины БЗ (" + Convert.ToChar(0x3B3) + "^2)" });
            newPlot.Series.Add(lineSeries);
            PlotModel = newPlot;
        }

        public DelegateCommand DrawCommand
        {
            get { return _drawCommand; }
        }

        public PlotModel PlotModel
        {
            get { return _plotModel; }
            set
            {
                if (Equals(value, _plotModel)) return;
                _plotModel = value;
                OnPropertyChanged("PlotModel");
            }
        }

        public AdditionalH AditionalH
        {
            get { return _aditionalH; }
        }
    }
}
