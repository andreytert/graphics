﻿using OxyPlot;
using OxyPlot.Axes;
using OxyPlot.Series;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Graphics.Formuls;
namespace Graphics.ViewModels
{
    public class Gamma1PlotViewModel : ViewModelBase
    {
        private DelegateCommand drawCommand;
        
        public Gamma1PlotViewModel()
        {
            gamma1 = new Gamma1();
            drawCommand = new DelegateCommand(Draw);
        }

        private PlotModel plotModel;
        public PlotModel PlotModel
        {
            get { return plotModel; }
            set
            {
                if (Equals(value, plotModel)) return;
                plotModel = value;
                OnPropertyChanged("PlotModel");
            }
        }

        private Gamma1 gamma1;

        private void Draw()
        {

            PlotModel newPlotModel = new PlotModel() { DefaultFontSize = 20, LegendFontSize = 20 };

            var ls = new LineSeries() { Title = Convert.ToChar(0x3B3).ToString() };
            newPlotModel.Axes.Add(new LogarithmicAxis(AxisPosition.Left, string.Format("коэффициент глубины БЗ ({0})", Convert.ToChar(0x3B3))) { UseSuperExponentialFormat = true });
            newPlotModel.Axes.Add(new LogarithmicAxis(AxisPosition.Bottom, "частота (f, Гц)") { UseSuperExponentialFormat = true });
            var range = RangeHelper.GenerateRange(3e7, 1e9, 1e4);
            foreach (var f in range)
            {
                double y = gamma1.CalculateUseF(f);
                ls.Points.Add(new DataPoint { X = f, Y = y });
            }
            

            newPlotModel.Series.Add(ls);
            PlotModel = newPlotModel;
        }

        public DelegateCommand DrawCommand
        {
            get { return drawCommand; }
        }
    }
}
