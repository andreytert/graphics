﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Graphics.Formuls;
using OxyPlot;
using OxyPlot.Axes;
using OxyPlot.Series;

namespace Graphics.ViewModels
{
    public class GammaEnergySecrecyPlotViewModel : ViewModelBase
    {
        private int _from;
        private int _to;
        private double _step;
        private PlotModel _plotModel;
        private GammaEnergySecrecy _formula;
        private DelegateCommand _drawCommand;
        private List<double> _gammas;

        public GammaEnergySecrecyPlotViewModel()
        {
            this._from = 0;
            this._to = 25;
            this._step = 0.05;
            this._plotModel = new PlotModel();
            this._formula = new GammaEnergySecrecy();
            this._drawCommand = new DelegateCommand(Draw);

        }

        LineStyle GetLineStyle(int idx)
        {
            switch (idx % 7)
            {
                case 0:
                    return LineStyle.Dash;
                case 1:
                    return LineStyle.LongDashDot;
                case 2:
                    return LineStyle.DashDot;
                case 3:
                    return LineStyle.LongDash;
                case 4:
                    return LineStyle.Solid;
                case 5:
                    return LineStyle.DashDashDot;
                case 6:
                    return LineStyle.Dot;
            }
            return LineStyle.Dash;
        }

        private void Draw()
        {
            PlotModel pl = new PlotModel();
            pl.Axes.Add(new LinearAxis(AxisPosition.Bottom, 0, 25, "Gamma"));
            pl.Axes.Add(new LinearAxis(AxisPosition.Left, 0, 25, "GammaES, Дб"));

            LineSeries ls = new LineSeries(){Title = "R = " + _formula.R.ToString()};
            pl.Series = PlotModel.Series;
            ls.LineStyle = GetLineStyle(pl.Series.Count);
            if (_step > 0)
            {
                var range = RangeHelper.GenerateRange(_from, _to, _step);
                for (double x = _from + _step; x <= _to; x += _step)
                {
                    double y = MathHelper.ToDb( _formula.Calculate(x) );
                    ls.Points.Add(new DataPoint{X = x, Y = y});
                }
            }
            pl.Series.Add(ls);

            this.PlotModel = pl;
        }

        public DelegateCommand DrawCommand
        {
            get { return _drawCommand; }
        }

        public PlotModel PlotModel
        {
            get { return _plotModel; }
            set
            {
                //if (Equals(value, _plotModel)) return;
                _plotModel = value;
                OnPropertyChanged("PlotModel");
            }
        }
        public int From
        {
            get { return _from; }
            set
            {
                _from = value;
                OnPropertyChanged("From");
            }
        }
        public int To
        {
            get { return _to; }
            set
            {
                _to = value;
                OnPropertyChanged("To");
            }
        }
        public double Step
        {
            get { return _step; }
            set
            {
                _step = value;
                OnPropertyChanged("Step");
            }
        }

        public GammaEnergySecrecy Formula
        {
            get { return _formula; }
        }
    }
}
