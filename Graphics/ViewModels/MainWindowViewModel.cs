﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Graphics.Plots;
using Graphics.Views;

namespace Graphics.ViewModels
{
    public class MainWindowViewModel : ViewModelBase
    {
        private Sigma1Plot _sigma1Plot;
        private Sigma2Plot _sigma2Plot;
        private Gamma1Plot gamma1Plot;
        private Gamma2Plot gamma2Plot;
        private POshibkiPlot _pOshibkiPlot;
        private AditionalHWithGammaDualRecivePlot _HWithGammaDualRecivePlot;
        private PErrorSeriesPlot _PErrorSeriesPlot;
        private AditionalHWithF1DualRecivePlot _aditionalHWithF1DualRecivePlot;
        private AditionalHWithF2DualRecivePlot _aditionalHWithF2DualRecivePlot;
        private AdditionalHWithGammaSingPlot additionalHWithGammaSingPlot;
        private AdditionalHWithF1SingPlot additionalHWithF1SingPlot;
        private AdditionalHWithF2SingPlot additionalHWithF2SingPlot;
        private PErrorSeriesDualPlot _PErrorSeriesDualPlot;
        private PErrorSeriesSinglePlot _pErrorSeriesSinglePlot;
        private AdditionalHWithF1SingDualSubPlot additionalHWithF1SingDualSubPlot;
        private AdditionalHWithF2SingDualSubPlot additionalHWithF2SingDualSubPlot;
        private AdditionalHWithFUseGamma2SignleDualSubPlot _additionalHWithFUseGamma2SignleDualSubPlot;
        private Table1View _table1View;
        private Table3d1View _table3d1View;
        private GammaEnergySecrecyPlot _gammaEnergySecrecyPlot;

        public MainWindowViewModel()
        {
            _sigma1Plot = new Sigma1Plot();
            _sigma2Plot = new Sigma2Plot();
            gamma1Plot = new Gamma1Plot();
            gamma2Plot = new Gamma2Plot();
            _pOshibkiPlot = new POshibkiPlot();
            _HWithGammaDualRecivePlot = new AditionalHWithGammaDualRecivePlot();
            _PErrorSeriesPlot = new PErrorSeriesPlot();
            _aditionalHWithF1DualRecivePlot = new AditionalHWithF1DualRecivePlot();
            _aditionalHWithF2DualRecivePlot = new AditionalHWithF2DualRecivePlot();
            additionalHWithGammaSingPlot = new AdditionalHWithGammaSingPlot();
            additionalHWithF1SingPlot = new AdditionalHWithF1SingPlot();
            additionalHWithF2SingPlot = new AdditionalHWithF2SingPlot();
            _PErrorSeriesDualPlot = new PErrorSeriesDualPlot();
            _pErrorSeriesSinglePlot = new PErrorSeriesSinglePlot();
            additionalHWithF1SingDualSubPlot = new AdditionalHWithF1SingDualSubPlot();
            additionalHWithF2SingDualSubPlot = new AdditionalHWithF2SingDualSubPlot();
            _additionalHWithFUseGamma2SignleDualSubPlot = new AdditionalHWithFUseGamma2SignleDualSubPlot();
            _table1View = new Table1View();
            _table3d1View = new Table3d1View();
            _gammaEnergySecrecyPlot = new GammaEnergySecrecyPlot();
        }

        public Sigma1Plot Sigma1Plot
        {
            get { return _sigma1Plot; }
        }

        public Sigma2Plot Sigma2Plot
        {
            get { return _sigma2Plot; }
        }
        
        public Gamma1Plot Gamma1Plot
        {
            get { return gamma1Plot; }
        }

        public Gamma2Plot Gamma2Plot
        {
            get { return gamma2Plot; }
        }

        public POshibkiPlot POshibkiPlot
        {
            get { return _pOshibkiPlot; }
        }

        public AditionalHWithGammaDualRecivePlot HWithGammaDualRecivePlot
        {
            get { return _HWithGammaDualRecivePlot; }
        }
        
        public AdditionalHWithGammaSingPlot AdditionalHWithGammaSingPlot
        {
            get { return additionalHWithGammaSingPlot; }
        }

        public PErrorSeriesPlot PErrorSeriesPlot
        {
            get { return _PErrorSeriesPlot; }
        }
        
        public AdditionalHWithF1SingPlot AdditionalHWithF1SingPlot
        {
            get { return additionalHWithF1SingPlot; }
        }

        public AditionalHWithF1DualRecivePlot AditionalHWithF1DualRecivePlot
        {
            get { return _aditionalHWithF1DualRecivePlot; }
        }

        public AditionalHWithF2DualRecivePlot AditionalHWithF2DualRecivePlot
        {
            get { return _aditionalHWithF2DualRecivePlot; }
        }
        
        public AdditionalHWithF2SingPlot AdditionalHWithF2SingPlot
        {
            get { return additionalHWithF2SingPlot; }
        }

        public PErrorSeriesDualPlot PErrorSeriesDualPlot
        {
            get { return _PErrorSeriesDualPlot; }
        }

        public PErrorSeriesSinglePlot PErrorSeriesSinglePlot
        {
            get { return _pErrorSeriesSinglePlot; }
        }

        public AdditionalHWithF1SingDualSubPlot AdditionalHWithF1SingDualSubPlot
        {
            get { return additionalHWithF1SingDualSubPlot; }
        }

        public AdditionalHWithF2SingDualSubPlot AdditionalHWithF2SingDualSubPlot
        {
            get { return additionalHWithF2SingDualSubPlot; }
        }

        public AdditionalHWithFUseGamma2SignleDualSubPlot AdditionalHWithFUseGamma2SignleDualSubPlot
        {
            get { return _additionalHWithFUseGamma2SignleDualSubPlot; }
        }

        public Table1View Table1View
        {
            get { return _table1View; }
        }

        public Table3d1View Table3d1View
        {
            get { return _table3d1View; }
        }

        public GammaEnergySecrecyPlot GammaEnergySecrecyPlot
        {
            get { return _gammaEnergySecrecyPlot; }
        }
    }
}
