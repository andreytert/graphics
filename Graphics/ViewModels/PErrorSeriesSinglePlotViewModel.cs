﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Graphics.Formuls;
using OxyPlot;
using OxyPlot.Axes;
using OxyPlot.Series;

namespace Graphics.ViewModels
{
    /// <summary>
    /// Approximate PError
    /// </summary>
    public class PErrorSeriesSinglePlotViewModel : ViewModelBase
    {
        private int _from;
        private int _to;
        private double _step;
        private PlotModel _plotModel;
        private PErrorSingle _formula;
        private DelegateCommand _drawCommand;

        private List<double> _gammas;

        public PErrorSeriesSinglePlotViewModel()
        {
            this._from = 1;
            this._to = 100000;
            this._step = 1;
            this._plotModel = new PlotModel();
            this._formula = new PErrorSingle();
            this._drawCommand = new DelegateCommand(Draw);

            this._gammas = new List<double>();
            _gammas.Add(1.1 * 1000);
            _gammas.Add(277.3);
            _gammas.Add(177.3);
            _gammas.Add(68.9);
            _gammas.Add(43.9);
            _gammas.Add(24.5);
            _gammas.Add(10.6);
            _gammas.Add(6.62);
            _gammas.Add(4.92);
            _gammas.Add(3.52);
            _gammas.Add(0.58);

        }

        LineStyle GetLineStyle(int idx)
        {
            switch (idx%7)
            {
                case 0:
                    return LineStyle.Dash;
                case 1:
                    return LineStyle.DashDashDot;
                case 2:
                    return LineStyle.DashDot;
                case 3:
                    return LineStyle.LongDash;
                case 4:
                    return LineStyle.LongDashDot;
                case 5:
                    return LineStyle.Solid;
                case 6:
                    return LineStyle.Dot;
            }
            return LineStyle.Dash;
        }

        private void Draw()
        {
            PlotModel pl = new PlotModel() { DefaultFontSize = 20, LegendFontSize = 20 };
            pl.Axes.Add(new LogarithmicAxis(AxisPosition.Bottom, "велечина отношения сигнал/шум (h^2)") { UseSuperExponentialFormat = true });
            pl.Axes.Add(new LogarithmicAxis(AxisPosition.Left, 0.0000001, 0.5, "вероятность ошибки (Pош)") { UseSuperExponentialFormat = true });

            foreach (var g in _gammas)
            {
                LineSeries ls = new LineSeries() { Title = Convert.ToChar(0x3B3)+" = " + g.ToString() };
                ls.LineStyle = GetLineStyle(pl.Series.Count);
                _formula.Gamma = g;
                if (_step > 0)
                {
                    var range = RangeHelper.GenerateRange(_from, _to, _step);
                    for (double x = _from + _step; x <= _to; x += _step)
                    {
                        ls.Points.Add(new DataPoint { X = x, Y = _formula.Calcute(x) });
                    }
                }
                pl.Series.Add(ls);
            }
            this.PlotModel = pl;
        }



        public DelegateCommand DrawCommand
        {
            get { return _drawCommand; }
        }

        public PlotModel PlotModel
        {
            get { return _plotModel; }
            set
            {
                //if (Equals(value, _plotModel)) return;
                _plotModel = value;
                OnPropertyChanged("PlotModel");
            }
        }
        public int Fromm
        {
            get { return _from; }
            set
            {
                _from = value;
                OnPropertyChanged("Fromm");
            }
        }
        public int To
        {
            get { return _to; }
            set
            {
                _to = value;
                OnPropertyChanged("To");
            }
        }
        public double Step
        {
            get { return _step; }
            set
            {
                _step = value;
                OnPropertyChanged("Step");
            }
        }
        public double Gamma
        {
            get { return _formula.Gamma; }
            set
            {
                _formula.Gamma = value;
                OnPropertyChanged("Gamma");
            }
        }

        public PErrorSingle Formula
        {
            get { return _formula; }
        }
    }
}
