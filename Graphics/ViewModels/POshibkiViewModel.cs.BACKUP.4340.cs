﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using OxyPlot;
using OxyPlot.Series;
using OxyPlot.Axes;

using Graphics.Formuls;

namespace Graphics.ViewModels
{
    class POshibkiViewModel:ViewModelBase
    {
        private int _from;
        private int _to;
        private double _step;
        private PlotModel _plotModel;
        private POshibki _formula;
        private DelegateCommand _drawCommand;

        

        public POshibkiViewModel()
        { 
            this._from=1;
            this._to = 100000;
            this._step = 1;
            this._plotModel = new PlotModel();
            this._formula = new POshibki();
            this._drawCommand = new DelegateCommand(Draw);
        }

        private void Draw()
        {
            PlotModel old = PlotModel;
            PlotModel pl = new PlotModel();
            pl.Series = old.Series;
            LineSeries ls = new LineSeries();
            pl.Axes.Add(new LogarithmicAxis(AxisPosition.Bottom, "h^2") { UseSuperExponentialFormat = true });
            pl.Axes.Add(new LogarithmicAxis(AxisPosition.Left, "Pош") { UseSuperExponentialFormat = true });
            if (_step > 0)
<<<<<<< HEAD
                for (double x = _from+_step; x <= _to; x += _step) {
                    double y = _formula.Calcute(x);
                    if (y < 1e-6)
                        break;
                    ls.Points.Add(new DataPoint { X = x, Y = y });
                }
            ls.Title = String.Format("Gamma = {0:F0}", Gamma);
=======
            {
                var range = RangeHelper.GenerateRange(_from, _to, _step);
                for (double x = _from + _step; x <= _to; x += _step)                    
                {
                    ls.Points.Add(new DataPoint { X = x, Y = _formula.Calcute(x) });
                }
            }



                
>>>>>>> 18e7cde60674f42828b45dd936d0dfd1d135d100
            pl.Series.Add(ls);
            
            PlotModel = pl;
        }



        public DelegateCommand DrawCommand
        {
            get { return _drawCommand; }
        }

        public PlotModel PlotModel
        {
            get { return _plotModel; }
            set
            {
                if (Equals(value, _plotModel)) return;
                _plotModel = value;
                OnPropertyChanged("PlotModel");
            }
        }
        public int Fromm
        {
            get { return _from; }
            set 
            {
                _from = value;
                OnPropertyChanged("Fromm");
                DrawCommand.Execute(null);
            }
        }
        public int To
        {
            get { return _to; }
            set
            {
                _to = value;
                OnPropertyChanged("To");
                DrawCommand.Execute(null);
            }
        }
        public double Step
        {
            get { return _step; }
            set
            {
                _step = value;
                OnPropertyChanged("Step");
            }
        }
        public double Gamma
        {
            get { return _formula.Gamma; }
            set
            {
                _formula.Gamma = value;
                OnPropertyChanged("Gamma");
                DrawCommand.Execute(null);
            }
        }
    }
}
