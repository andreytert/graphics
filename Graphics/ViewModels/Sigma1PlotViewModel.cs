﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Graphics.Formuls;
using OxyPlot;
using OxyPlot.Axes;
using OxyPlot.Series;

namespace Graphics.ViewModels
{
    public class Sigma1PlotViewModel : ViewModelBase
    {
        private PlotModel _plotModel;
        private Sigma1 _sigma1;
        private DelegateCommand _drawCommand;

        public Sigma1PlotViewModel()
        {
            _sigma1 = new Sigma1();
            _drawCommand = new DelegateCommand(Draw);
        }

        private void Draw()
        {
            PlotModel newPlot = new PlotModel() { LegendFontSize=20 };
            LineSeries lineSeries = new LineSeries() { Title = String.Format("{0}1", Convert.ToChar(0x03C3))};            
            var range = RangeHelper.GenerateRange(3e7, 1e9, 1e7);
            foreach (var x in range)
            {
                lineSeries.Points.Add(new DataPoint(x, _sigma1.Calculate(x)));
            }
            newPlot.Axes.Add(new LogarithmicAxis(AxisPosition.Left, String.Format("СКО флуктуаций фазового фронта волны ({0}, рад)", Convert.ToChar(0x03C3))) { FontSize = 20 });
            newPlot.Axes.Add(new LogarithmicAxis(AxisPosition.Bottom, String.Format("Частота (f, Гц)", Convert.ToChar(0x03C3))) { UseSuperExponentialFormat = true, FontSize=20 });
            newPlot.Series.Add(lineSeries);
            PlotModel = newPlot;
        }

        public PlotModel PlotModel
        {
            get { return _plotModel; }
            set
            {
                if (Equals(value, _plotModel)) return;
                _plotModel = value;
                OnPropertyChanged("PlotModel");
            }
        }

        public DelegateCommand DrawCommand
        {
            get { return _drawCommand; }
        }
    }
}
