﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Graphics.Formuls;

namespace Graphics.ViewModels
{
    class Table1ViewModel : ViewModelBase
    {
        private List<object[]> _records;

        public Table1ViewModel()
        {
            _records = new List<object[]>();
            Calc(); 
        }

        public List<object[]> Records
        {
            get { return _records; }
        }

        private void Calc()
        {
            #region F
            object[] f0 = new object[12] { "f0, Гц", 1e9, 5 * 1e8, 4 * 1e8, 2.5 * 1e8, 2 * 1e8, 1.5 * 1e8, 1e8, 8 * 1e7, 7 * 1e7, 6 * 1e7, 3 * 1e7 };
            _records.Add(f0);
            #endregion

            #region sigma1
            object[] sigma11 = new object[12];
            Sigma1 sigma10 = new Sigma1();
            sigma11[0] = "Сигма 1";
            for (int i = 1; i < 12; i++)
                sigma11[i] = sigma10.Calculate((double)f0[i]);
            _records.Add(sigma11);
            #endregion
            #region gamma1
            object[] gamma1 = new object[12];
            Gamma1 gamma11 = new Gamma1();
            gamma1[0] = "Гамма 1";
            for (int i = 1; i < 12; i++)
                gamma1[i] = gamma11.CalculateUseF((double)f0[i]);
            _records.Add(gamma1);
            #endregion

            #region sigma2u
            object[] sigma222 = new object[12];
            Sigma2u sigma2u = new Sigma2u();
            sigma222[0] = "Сигма 2у";
            for (int i = 1; i < 12; i++)
                sigma222[i] = sigma2u.Calculate((double)f0[i]);
            _records.Add(sigma222);
            #endregion
            #region gamma2u
            object[] gamma2u = new object[12];
            Gamma2 gamma22u = new Gamma2();
            gamma2u[0] = "Гамма 2у";
            for (int i = 1; i < 12; i++)
                gamma2u[i] = gamma22u.CalculateUseF((double)f0[i]);
            _records.Add(gamma2u);
            #endregion


            #region sigma21
            object[] sigma21 = new object[12];
            Sigma2 sigma2 = new Sigma2(25, 1000);
            sigma21[0] = "Сигма(25 градусов)";
            for (int i = 1; i < 12; i++)
                sigma21[i] = sigma2.Calculate((double)f0[i]);
            _records.Add(sigma21);
            #endregion
            #region sigma22
            object[] sigma22 = new object[12];
            Sigma2 sigma1 = new Sigma2(1e3, 25);
            sigma22[0] = "Сигма(0 градусов)";
            for (int i = 1; i < 12; i++)
                sigma22[i] = sigma1.Calculate((double)f0[i]);
            _records.Add(sigma22);
            #endregion

            OnPropertyChanged("Records");
        }

    }
}
