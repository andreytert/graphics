﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Graphics.Formuls;

namespace Graphics.ViewModels
{
    public class Table3d1ViewModel : ViewModelBase
    {
        private List<object[]> _records;
        public Table3d1ViewModel()
        {
            _records = new List<object[]>();
            Calculate();
        }
        private void Calculate()
        {
            object[] f0 = new object[12]
                {"f0, Гц", 1e9, 5*1e8, 4*1e8, 2.5*1e8, 2*1e8, 1.5*1e8, 1e8, 8*1e7, 7*1e7, 6*1e7, 3*1e7};
            _records.Add(f0);
            
            object[] sigmaFi2 = new object[12];
            Sigma2 sigma2 = new Sigma2(25, 1000);
            sigmaFi2[0] = "Sigma2Fi2";
            for (int i = 1; i < 12; i++)
                sigmaFi2[i] = sigma2.Calculate((double)f0[i]); 
            _records.Add(sigmaFi2);

            object[] gamma = new object[12];
            gamma[0] = "gamma";
            GammaUseSigma2 gammaUseSigma2 = new GammaUseSigma2();
            for (int i = 1; i < 12; i++)
                gamma[i] = gammaUseSigma2.CalculateUseSigma((double) sigmaFi2[i]);
            _records.Add(gamma);

            object[] deltaRo = new object[12];
            deltaRo[0] = "deltaRo";
            for (int i = 1; i < 12; i++)
                deltaRo[i] = 1e3/(double)sigmaFi2[i];
            _records.Add(deltaRo);

            object[] deltaRoAdditional = new object[12];
            deltaRoAdditional[0] = "deltaRo доп";
            for (int i = 1; i < 12; i++)
                deltaRoAdditional[i] = 0.832*(double)deltaRo[i];
            _records.Add(deltaRoAdditional);

            AdditionalH additionalH = new AdditionalH(0.7);

            object[] hAdditional2 = new object[12];
            hAdditional2[0] = "Hдоп2";
            for (int i = 1; i < 12; i++)
                hAdditional2[i] = MathHelper.ToDb(additionalH.CalculateWithGammaDual((double) gamma[i]));
            _records.Add(hAdditional2);

            object[] hAdditional1 = new object[12];
            hAdditional1[0] = "Hдоп1";
            for (int i = 1; i < 12; i++)
                hAdditional1[i] = MathHelper.ToDb(additionalH.CalculateUseGammaSingle((double) gamma[i]));
            _records.Add(hAdditional1);

            object[] hAddtitionalSubstract = new object[12];
            hAddtitionalSubstract[0] = "Hдоп1 - Hдоп2";
            for (int i = 1; i < 12; i++)
                hAddtitionalSubstract[i] = (double)hAdditional1[i] - (double)hAdditional2[i];
           _records.Add(hAddtitionalSubstract);
 
           OnPropertyChanged("Records");
        }

        public List<object[]> Records
        {
            get { return _records; }
        }
    }
}
