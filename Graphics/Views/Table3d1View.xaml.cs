﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Graphics.ViewModels;

namespace Graphics.Views
{
    /// <summary>
    /// Interaction logic for Table3d1View.xaml
    /// </summary>
    public partial class Table3d1View : UserControl
    {
        private Table3d1ViewModel model = new Table3d1ViewModel();
        public Table3d1View()
        {
            InitializeComponent();
            DataContext = model;
        }
    }
}
